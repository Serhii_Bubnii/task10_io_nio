package com.bubnii.model.task3;

import com.bubnii.view.ViewPrint;
import org.apache.commons.lang3.time.StopWatch;

import java.io.*;
import java.net.URISyntaxException;
import java.util.ResourceBundle;

public class Writer {

    private ViewPrint printObject = new ViewPrint();

    public void usualWriting() {
        try (FileInputStream inputStream =
                     new FileInputStream(
                             new File(this.getClass().getResource("/40MB.txt").toURI()));
             FileOutputStream outputStream =
                     new FileOutputStream(
                             new File(this.getClass().getResource("/text.txt").toURI()))) {
            int c;
            while ((c = inputStream.read()) != -1) {
                outputStream.write(c);
            }
            printObject.print("Usual writing");
            printObject.print("Write from file is complete");
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }

    //default buffer size = 8192
    public void bufferedWriting() {
        try (BufferedInputStream inputStream =
                     new BufferedInputStream(
                             new FileInputStream(
                                     new File(this.getClass().getResource("/100MB.txt").toURI())));
             BufferedOutputStream bufferedOutputStream =
                     new BufferedOutputStream(
                             new FileOutputStream(
                                     new File(this.getClass().getResource("/text.txt").toURI())))) {
            int c;
            while ((c = inputStream.read()) != -1) {
                bufferedOutputStream.write(c);
            }
            printObject.print("Buffered writing");
            printObject.print("Write from file is complete");
        } catch (URISyntaxException | IOException e) {
            e.printStackTrace();
        }
    }

    public void bufferedWritingToSize(int size) {
        try (BufferedInputStream inputStream =
                     new BufferedInputStream(
                             new FileInputStream(
                                     new File(this.getClass().getResource("/200MB.txt").toURI())));
             BufferedOutputStream bufferedOutputStream =
                     new BufferedOutputStream(
                             new FileOutputStream(
                                     new File(this.getClass().getResource("/text.txt").toURI())), size)) {
            int c;
            while ((c = inputStream.read()) != -1) {
                bufferedOutputStream.write(c);
            }
            printObject.print("Buffered writing to size " + size);
            printObject.print("Write from file is complete");
        } catch (URISyntaxException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        new Writer().bufferedWritingToSize(1024);
        stopWatch.stop();
        long timeElapsed1 = stopWatch.getTime();
        System.out.println(timeElapsed1);
    }
}
