package com.bubnii.model.task3;

import com.bubnii.view.ViewPrint;
import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;

public class Reader {

    private ViewPrint printObject = new ViewPrint();

    public void usualReading() {
        try (FileInputStream inputStream =
                     new FileInputStream(
                             new File(this.getClass().getResource("/40MB.txt").toURI()))) {
            while (inputStream.read() != -1) {
            }
            printObject.print("Usual reading");
            printObject.print("Read from file is complete");
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }

    //default buffer size = 8192
    public void bufferedReading() {
        try (BufferedInputStream inputStream =
                     new BufferedInputStream(
                             new FileInputStream(
                                     new File(this.getClass().getResource("/200MB.txt").toURI())))) {
            while (inputStream.read() != -1) {
            }
            printObject.print("Buffered reading");
            printObject.print("Read from file is complete");
        } catch (URISyntaxException | IOException e) {
            e.printStackTrace();
        }
    }

    public void bufferedReadingToSize(int size) {
        try (BufferedInputStream inputStream =
                     new BufferedInputStream(
                             new FileInputStream(
                                     new File(this.getClass().getResource("/200MB.txt").toURI())), size)) {
            while (inputStream.read() != -1) {

            }
            printObject.print("Buffered reading to size " + size);
            printObject.print("Read from file is complete");
        } catch (URISyntaxException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
       StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        new Reader().bufferedReadingToSize(24576);
        stopWatch.stop();
        long timeElapsed1 = stopWatch.getTime();
        System.out.println(timeElapsed1);
    }
}
