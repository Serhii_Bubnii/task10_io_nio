package com.bubnii.model.task7;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ReadWriteSimultaneously {
    public void readWriteFile() {
        String filePath = "c:\\Users\\bubni\\IdeaProjects\\task10_io_nio\\src\\main\\resources\\text.txt";
        try {
            Path path = Paths.get(filePath);
            FileChannel channel = FileChannel.open(path);
            ByteBuffer buffer = ByteBuffer.allocate(20);
            int i = channel.read(buffer);
            while (i != -1) {
                System.out.println(i);
                buffer.flip();
                while (buffer.hasRemaining()) {
                    System.out.println((char) buffer.get());
                }
                System.out.println(" ");
                buffer.clear();
                i = channel.read(buffer);
            }
            channel.close();

            FileOutputStream fos = new FileOutputStream(filePath);
            FileChannel fileChannel = fos.getChannel();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new ReadWriteSimultaneously().readWriteFile();
    }
}
