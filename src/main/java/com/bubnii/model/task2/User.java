package com.bubnii.model.task2;

import java.io.*;

public class User implements Serializable {

    private String name;
    private String lastName;
    private int age;
    private String country;
    private transient String surName;
    static int staticValue;

    public User(String name, String lastName, String surName, int age, String country) {
        this.name = name;
        this.lastName = lastName;
        this.surName = surName;
        this.age = age;
        this.country = country;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", country='" + country + '\'' +
                ", surName='" + surName + '\'' +
                '}';
    }
}
