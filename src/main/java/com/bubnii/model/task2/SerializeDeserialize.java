package com.bubnii.model.task2;

import com.bubnii.view.ViewPrint;

import java.io.*;

public class SerializeDeserialize {

    private User user = new User("Bob", "Willis", "Salmiyyeh", 43, "US");
    private ViewPrint printObject = new ViewPrint();

    public void serializeObject() {
        user.staticValue = 100;
        try (FileOutputStream fileOutputStream = new FileOutputStream("file");
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {
            objectOutputStream.writeObject(user);
            printObject.print(user);
            printObject.print("Static value = " + User.staticValue);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deserializeObject() {
        user.staticValue = 0;
        try (FileInputStream fileInputStream = new FileInputStream("file");
             ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)) {
            User newUser = (User) objectInputStream.readObject();
            printObject.print(newUser);
            printObject.print("Static value = " + User.staticValue);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
