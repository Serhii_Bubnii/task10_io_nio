package com.bubnii.model.task6;

import java.io.File;
import java.util.Objects;

public class ContentDirectory {

    public static void recursivePrint(File[] arrObjects, int level) {
        for (File object : arrObjects) {
            for (int i = 0; i < level; i++)
                System.out.print("\t");
            if (object.isFile())
                System.out.println(object.getName());
            else if (object.isDirectory()) {
                System.out.println("[" + object.getName() + "]");
                recursivePrint(Objects.requireNonNull(object.listFiles()), level + 1);
            }
        }
    }
}