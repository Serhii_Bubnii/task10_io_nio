package com.bubnii.controller;

import com.bubnii.model.task2.SerializeDeserialize;
import com.bubnii.model.task3.Reader;
import com.bubnii.model.task3.Writer;

public class Controller {

    private SerializeDeserialize serializeDeserialize = new SerializeDeserialize();
    private Reader reader = new Reader();
    private Writer writer = new Writer();

    public void callSerializeObject() {
        serializeDeserialize.serializeObject();
    }

    public void callDeserializeObject() {
        serializeDeserialize.deserializeObject();
    }

    public void callUsualReading() {
        reader.usualReading();
    }

    public void callBufferedReading() {
        reader.bufferedReading();
    }

    public void callBufferedReadingToSize(int size) {
        reader.bufferedReadingToSize(size);
    }

    public void callUsualWriting() {
        writer.usualWriting();
    }

    public void callBufferedWriting() {
        writer.bufferedWriting();
    }

    public void callBufferedWritingToSize(int size) {
        writer.bufferedWritingToSize(size);
    }
}
