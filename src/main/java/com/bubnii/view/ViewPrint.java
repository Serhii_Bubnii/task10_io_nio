package com.bubnii.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ViewPrint {
    private static Logger logger = LogManager.getLogger(ViewPrint.class);

    public void print(Object object) {
        logger.info(object);
    }
}