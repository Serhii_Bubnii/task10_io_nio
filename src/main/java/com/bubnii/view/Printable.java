package com.bubnii.view;

public interface Printable {
    void print();
}
