package com.bubnii.view;

import com.bubnii.controller.Controller;
import com.bubnii.model.task4.ReverseInputStream;
import com.bubnii.model.task6.ContentDirectory;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {
    private Controller controller = new Controller();
    private ViewPrint printView = new ViewPrint();
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Scanner input;

    public View() {
        input = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "\t1 - Show serialization and deserialization");
        menu.put("2", "\t2 - Show usual reading");
        menu.put("3", "\t2 - Show usual writing");
        menu.put("4", "\t2 - Show usual buffered reading");
        menu.put("5", "\t2 - Show usual buffered writing");
        menu.put("6", "\t2 - Show usual buffered reading to size");
        menu.put("7", "\t2 - Show usual buffered writing to size");
        menu.put("8", "\t2 - Show reverse input stream");
        menu.put("9", "\t2 - Show print contents directory");
        menu.put("10", "\t2 - /////////////");
        menu.put("Q", "\tQ - exit");
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::showSerializeDeserialize);
        methodsMenu.put("2", this::showUsualReading);
        methodsMenu.put("3", this::showUsualWriting);
        methodsMenu.put("4", this::showBufferedReading);
        methodsMenu.put("5", this::showBufferedWriting);
        methodsMenu.put("6", this::showBufferedReadingToSize);
        methodsMenu.put("7", this::showBufferedWritingToSize);
        methodsMenu.put("8", this::showReverseInputStream);
        methodsMenu.put("9", this::showPrintContentsDirectory);
    }

    private void showSerializeDeserialize() {
        controller.callSerializeObject();
        controller.callDeserializeObject();
    }

    private void showUsualReading() {
        controller.callUsualReading();
    }

    private void showUsualWriting() {
        controller.callUsualWriting();
    }

    private void showBufferedReading() {
        controller.callBufferedReading();
    }

    private void showBufferedWriting() {
        controller.callBufferedWriting();
    }

    private void showBufferedReadingToSize() {
        printView.print("Enter the buffer size");
        int size = input.nextInt();
        controller.callBufferedReadingToSize(size);
    }

    private void showBufferedWritingToSize() {
        printView.print("Enter the buffer size");
        int size = input.nextInt();
        controller.callBufferedWritingToSize(size);
    }

    private void showReverseInputStream() {
        try {
            printView.print("Enter the data:");
            String str = input.nextLine();
            InputStream inputStream = new ByteArrayInputStream(str.getBytes());
            inputStream = new ReverseInputStream(inputStream);
            final byte[] buffer = new byte[4];
            for (int n = inputStream.read(buffer, 0, buffer.length);
                 n >= 0;
                 n = inputStream.read(buffer, 0, buffer.length))
                System.out.print(new String(buffer, 0, n));
            System.out.println();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showPrintContentsDirectory() {
        String maindirpath = "c:\\Users\\bubni\\IdeaProjects\\task10_io_nio";
        File maindir = new File(maindirpath);
        if (maindir.exists() && maindir.isDirectory()) {
            File[] arr = maindir.listFiles();
            printView.print("**********************************************");
            printView.print("Files from main directory : " + maindir);
            printView.print("**********************************************");
            ContentDirectory.recursivePrint(arr, 0);
        }
    }

    public void show() {
        String keyMenu;
        do {
            printView.print("------------------------------------------------------------------------------------------");
            outputMenu();
            printView.print("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        printView.print("MENU:");
        for (String str : menu.values()) {
            printView.print(str);
        }
    }
}
